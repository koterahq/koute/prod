# Koute
Koute. Routing in Fusion supercharged.

Koute is a lightweight and minimal routing library for `Fusion@0.1.1-beta`. Learn more about Koute at [here](https://kotera.7kayoh.net).

> Koute only supports Rojo@7.2.1 or above. Those below the supported version will fail to have the source file synced properly.

> `Koute@1.1.0` will not support `Fusion@0.1.1-beta`, only supporting versions of `Fusion~0.2.0`.

| Installation Methods | Instructions |
| ------------- | ------------- |
| Wally | Append `Koute = "koterahq/koute~1.0.3"` into `wally.toml` |
| Wally (with `Fusion~0.2.0`) | Append `Koute = "koterahq/koute~1.1.0` into `wally.toml` |
| Binary (`.rbxm`) | Download built binary from the [latest release](https://gitlab.com/koterahq/koute/prod/-/releases) |
| Source Code | Download source code from the [latest release](https://gitlab.com/koterahq/koute/prod/-/releases) |

> This page may not be up to date with the latest release, you are advised to check [wally.run](https://wally.run/package/7kayoh/koute) for information regarding the latest release available.

`MIT Licensed | Copyright 2023 KoteraHQ and 7kayoh`